﻿using System;
using Enemy;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private RectTransform win;
        [SerializeField] private RectTransform lose;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        public static GameManager Instance { get; private set; }

        
        public PlayerSpaceship spawnedPlayerShip;
        
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
            
            startButton.onClick.AddListener(OnStartButtonClicked);
            
           
        }

        public void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            win.gameObject.SetActive(false);
            lose.gameObject.SetActive(false);
            StartGame();
        }

        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            SoundManager.Instance.PlayBGM();
        }
        
        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            var spawnedEnemySpaceship = Instantiate(enemySpaceship);
            spawnedEnemySpaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawnedEnemySpaceship.OnExploded += OnEnemySpaceshipExploded;
            
        }

        private void OnEnemySpaceshipExploded()
        {
            scoreManager.SetScore(1);
            scoreManager.Victory();
            win.gameObject.SetActive(true);
            DestroyRemainingShips();
        }

        private void Restart()
        {
            DestroyRemainingShips();
            lose.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }
        }

        public void NextLevel()
        {
            SceneManager.LoadScene("Game 2");
        }

        public void MainMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }

    }
}
